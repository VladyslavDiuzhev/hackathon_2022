
from random import random
import numpy as np
import cv2

import time

import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile, QoSReliabilityPolicy, QoSHistoryPolicy

from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import OccupancyGrid
from  sensor_msgs.msg import Image
from cv_bridge import CvBridge

from nav2_simple_commander.robot_navigator import BasicNavigator, TaskResult
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy

from pyzbar.pyzbar import decode


MAP_START_POINT = (25.0, -25.0)

class QRReaderSubscriber(Node):
    def __init__(self):
        super().__init__('image_subscriber')
        qos_profile = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        self.subscription = self.create_subscription(
            Image,
            'camera/image_raw',
            self.listener_callback,
            qos_profile
            )
        self.subscription  # prevent unused variable warning
        self.recognized_qr = {}

    def listener_callback(self, msg):
        bridge = CvBridge()
        cv_image = bridge.imgmsg_to_cv2(msg, desired_encoding='passthrough')
        # print(cv_image)
        frame = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
        out = self.qr_det(frame)
        if len(out) > 0:
            for dec in out:
                msg = dec.data.decode('UTF-8')
                splited_msg = msg.split('.')
                if int(splited_msg[0]) not in self.recognized_qr.keys():
                    print("Recognize new QR: {}".format(msg))
                    self.recognized_qr[int(splited_msg[0])] = splited_msg[1]

    def qr_det(self, frame):
        result = decode(frame)
        return result

    def read_qr_code(self, frame):
        ## Функция для обнаружения и расшифровки

        ## frame - это обыкновенный фрейм opencv. Никакой обработки он не подвергается, 
        ##          поиск происходит на исходном изображении ( подразумевается что в 
        ##          функцию уже подается обработанное изображение)
        try:
            detect = cv2.QRCodeDetector()
            ## info - расшифровка содержимого QR кода
            ## points - координаты углов в СК изображения
            ## output_frame - нормализованное и отбинаренное изображение QR кода
            info, points, output_frame = detect.detectAndDecode(frame)
            ## Это банальная проверка на ошибки. Почему-то иногда выдает координаты углов без найденного QR кода
            if ((points is None) or (output_frame is None)): 
                return False, None
            else:
                return True, [info, points, output_frame]

        except Exception as e:
            print(e)
            return False, None

class CostmapSubscriber(Node):
    def __init__(self):
        super().__init__('global_costmap_subscriber')
        self.occupancy_subscription = self.create_subscription(OccupancyGrid, '/global_costmap/costmap', self.occupancy_callback, 10)
        self.costmap = np.zeros(1)
        self.shape = (0, 0)
        self.resolution = 1

    def occupancy_callback(self, msg):
        """

        The global costmap subscriber callback function refreshes the list of accessible waypoints. It sorts them and
        saves them in the self.sorted_accessible_waypoints variable.

        :param msg: OccupancyGrid message. Includes map metadata and an array with the occupancy probability values
        :return: None
        """
        self.costmap = np.array(msg.data)  # download the occupancy grid
        
        self.shape = (msg.info.height, msg.info.width)  # get the current map width and height
        self.resolution = msg.info.resolution  # get the resolution

        # reshape the data so it resembles the map shape
        self.costmap = np.flip(np.reshape(self.costmap, (self.shape[0], self.shape[1])), axis=1)
        


class MapExplorer():
    def __init__(self, navigator: BasicNavigator, map_subscriber: Node):

        self.accessible_waypoints = np.array([])
        self.sorted_accessible_waypoints = np.array([])
        self.occupancy_value = np.array([])

        self.navigator = navigator
        self.map_subscriber = map_subscriber

    def update_map(self):
        rclpy.spin_once(self.map_subscriber)
        rclpy.spin_once(self.map_subscriber)

        # reshape the data so it resembles the map shape
        # import matplotlib.pyplot as plt
        # plt.imshow(self.map_subscriber.costmap)
        # plt.show()

        # print('Costmap params:', self.current_map_shape,  self.current_map_resolution)

        self.is_started = True

    def calculate_accesible_waypoints(self):

        # Here we go through every waypoint and save the ones that are accessible.
        # An accessible waypoint is one which has no obstacles, and has few or no unknown squares in the vicinity.
        self.accessible_waypoints = np.array([])
        self.occupancy_value = np.array([])
        for waypoint in self.waypoints:
            try:
                occupancy_grid_coordinates = [int((waypoint[0]) / self.map_subscriber.resolution), int((waypoint[1]) / self.map_subscriber.resolution)]
                # print(occupancy_grid_coordinates)
                conv = self.convolute(self.map_subscriber.costmap, occupancy_grid_coordinates, size=5)  # perform convolution

                # if the convolution returns True, it means the WP is accessible, so it is stored in
                # self.accessible_waypoints
                if conv[0]:
                    self.accessible_waypoints = np.append(self.accessible_waypoints, waypoint)
                    self.occupancy_value = np.append(self.occupancy_value, conv[1])
            # because the waypoint array is over-sized, we need to remove the values that are out of range
            except IndexError:
                pass

        # reshape the accessible waypoints array to shape (n, 2)
        self.accessible_waypoints = self.accessible_waypoints.reshape((-1, 2))
        # print(self.current_global_costmap)

        # Sorting waypoints according to occupancy value. This allows the robot to prioritize the waypoints with
        # more uncertainty (it wont access the areas that are completely clear, thus going to the discovery frontier)
        occupancy_value_idxs = self.occupancy_value.argsort()
        self.sorted_accessible_waypoints = self.accessible_waypoints[occupancy_value_idxs[::-1]]

        # At the beginning, when all values are uncertain, we add some hardcoded waypoints so it begins to navigate
        # and has time to discover accessible areas
        if np.size(self.sorted_accessible_waypoints) == 0:
            self.sorted_accessible_waypoints = np.array([[0.1, 0.0], [0.0, 0.1], [-0.1, 0.0], [0.0, -0.1]])

        # Once we have the new waypoints, they are saved in self.sorted_accessible_waypoints for use by the Navigator
        # client
        print('Accessible waypoints have been updated...')

    @staticmethod
    def convolute(data, coordinates, size=3, threshold=27):
        """
        This function calculates the average occupancy probability at 'coordinates' for an area of size (size x size)
        around said point.

        :param data: Occupancy Grid Data (shaped to (x, y) map dimensions)
        :param coordinates: the coordinates of the OccupancyGrid to convolute around
        :param size: size of the kernel
        :param threshold: threshold of accessibility
        :return: True or False, depending on whether the waypoint is accessible or not.
        :return: average: average occupancy probability of the convolution
        """
        sum = 0
        for x in range(int(coordinates[0] - size / 2), int(coordinates[0] + size / 2)):
            for y in range(int(coordinates[1] - size / 2), int(coordinates[1] + size / 2)):
                # if the area is unknown, we add 100 to sum.
                if data[x, y] == -1:
                    sum += 100
                # if occupancy state is above 50 (occupied), we add 1M to the sum so that the robot DOES NOT
                # access areas near walls.
                elif data[x, y] > 50:
                    sum += 1000000
                # if the occupancy state is below 50 and known, just add the value to sum.
                else:
                    sum += data[x, y]

        # average value for the square is computed
        average = sum / (size * size)
        if average < threshold:
            # if the average of the squares is below the threshold, the waypoint is accessible
            return True, average
        else:
            # if the average is above the threshold, the waypoint has either too many unknowns, or an obstacle
            return False, average

    def generate_list_of_waypoints(self, step):
        """

        Generates a grid of waypoints of size ('n_of_waypoints_x' * 'n_of_waypoints_y') and step size 'step'

        :param n_of_waypoints: number of total waypoints to generate per side
        :param step: float resolution of the waypoints
        :return waypoints: 2D numpy array of a list of coordinates of size dim x 2,
        where dim is the number of waypoints
        """
        n_of_waypoints_x = int(self.map_subscriber.shape[0]*self.map_subscriber.resolution/step)
        n_of_waypoints_y = int(self.map_subscriber.shape[1]*self.map_subscriber.resolution/step)

        self.waypoints = np.zeros((n_of_waypoints_x * n_of_waypoints_y, 2))

        i = 0
        for index_x in range(n_of_waypoints_x):
            for index_y in range(n_of_waypoints_y):
                self.waypoints[i] = [float(index_x)*step, float(index_y)*step]
                i += 1

        print("Grid of waypoints has been generated.")
        return self.waypoints


def go_to_the_next_point(navigator, point, image_subscriber):
    """
    :param publisher: publisher object that that is publishing in /goal_pose
    :param point: np.ndarray message object
    :return: nothing
    """
    goal_pose = PoseStamped()
    goal_pose.header.frame_id = 'map'
    goal_pose.header.stamp = navigator.get_clock().now().to_msg()
    goal_pose.pose.position.x = -point[1] + MAP_START_POINT[0]
    goal_pose.pose.position.y = point[0] + MAP_START_POINT[1]
    goal_pose.pose.position.z = 0.0
    goal_pose.pose.orientation.w = 1.0

    navigator.cancelTask()
    navigator.goToPose(goal_pose)

    print('Continue move and explore!')

    while not navigator.isTaskComplete():
        rclpy.spin_once(image_subscriber)
        feedback = navigator.getFeedback()

    result = navigator.getResult()

    if result == TaskResult.SUCCEEDED:
        print('Goal succeeded!')
    elif result == TaskResult.CANCELED:
        print('Goal was canceled!')
    elif result == TaskResult.FAILED:
        print('Goal failed!')
    else:
        print('Goal has an invalid return status!')

def main(args=None):
    rclpy.init(args=args)
    MAX_BUF_SIZE = 10

    navigator = BasicNavigator()
    costmap_subscriber = CostmapSubscriber()
    image_subscriber = QRReaderSubscriber()
    map_explorer = MapExplorer(navigator, costmap_subscriber)

    choosed_points = []

    while rclpy.ok():
        map_explorer.update_map()
        map_explorer.generate_list_of_waypoints(step=0.25)
        map_explorer.calculate_accesible_waypoints()
        
        next_point = map_explorer.sorted_accessible_waypoints[0]
        # print(next_point)
        if len(choosed_points)==0:
            choosed_points.append(next_point)
        else:
            for up in choosed_points:
                added = False
                for p in map_explorer.sorted_accessible_waypoints:
                    if np.linalg.norm(p-up)>0.4:
                        next_point = p
                        choosed_points.append(p)
                        if len(choosed_points)>MAX_BUF_SIZE:
                            choosed_points = choosed_points[len(choosed_points)-MAX_BUF_SIZE:]
                        added = True
                        break
                if added:
                    break
        go_to_the_next_point(navigator, next_point, image_subscriber)
        choosed_points.append(next_point)

    image_subscriber.destroy_node()
    # Shutdown nodes
    costmap_subscriber.destroy_node()
    # publisher.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
    