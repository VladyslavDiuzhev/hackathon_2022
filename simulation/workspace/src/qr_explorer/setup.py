from setuptools import setup

package_name = 'qr_explorer'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='texnoman',
    maintainer_email='texnoman@todo.todo',
    description='TODO: Package description',
    license='MIT',
    entry_points={
        'console_scripts': [
            'qr_explorer = qr_explorer.qr_explorer:main'
        ],
    },
)
